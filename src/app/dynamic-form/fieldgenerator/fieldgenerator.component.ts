import { Component, OnInit,Input} from '@angular/core';
import { DeleteitemService } from "../../deleteitem.service";

@Component({
	selector: '[app-fieldgenerator]',
	templateUrl: './fieldgenerator.component.html',
	styleUrls: ['./fieldgenerator.component.css']
})
export class FieldgeneratorComponent implements OnInit {
	constructor(
		private dservice:DeleteitemService
	) { }
	@Input() field:any;
	@Input() form:any;
	@Input() editstatus:boolean = false;
	ngOnInit(): void {

	}
	deleteField(eid:number){
		var data = {"eid":eid}
		this.dservice.deleteitemEvent.emit(data);
	}
	resputility(coltype:string){
		if(coltype == "col-2"){
			return "col-sm-6 mt-4"
		}else{
			return "col"
		}
	}
	
}
