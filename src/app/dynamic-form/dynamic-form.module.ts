import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FieldgeneratorComponent } from './fieldgenerator/fieldgenerator.component';
import { DynamicFormComponent } from './dynamic-form/dynamic-form.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
	declarations: [
		FieldgeneratorComponent,
		DynamicFormComponent
	],
	imports: [
		CommonModule,
		ReactiveFormsModule,

	],
	exports:[
		DynamicFormComponent
	]
})
export class DynamicFormModule { }
