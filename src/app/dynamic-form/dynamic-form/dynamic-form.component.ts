import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
	selector: '[app-dynamic-form]',
	templateUrl: './dynamic-form.component.html',
	styleUrls: ['./dynamic-form.component.css']
})
export class DynamicFormComponent implements OnInit {
	@Output() onEdit = new EventEmitter();
	@Input() fields: any[] = [];
	public form!: FormGroup;
	public is_editing:boolean = false;

	constructor() {
	}

	onEditClick(){
		this.is_editing = !this.is_editing;
		this.onEdit.emit()

	}
	onEditing(){
		if(this.is_editing){
			return "btn-clear"
		}else{
			return "btn-edit"
		}
	}
	ngOnInit(): void {
		let opts:any = {}
		for (let f of this.fields) {
			console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
			console.log(f)

			for (let opt of f.elements) {
				opts[opt.element_id] = new FormControl("",Validators.required);

			}
		}
		console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!11111")
		this.form = new FormGroup(opts);
		console.log("*****************88888")
		console.log(this.form);

	}


}
