import { TestBed } from '@angular/core/testing';

import { DeleteitemService } from './deleteitem.service';

describe('DeleteitemService', () => {
  let service: DeleteitemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DeleteitemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
