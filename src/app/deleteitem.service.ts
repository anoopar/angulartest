import { Injectable,EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DeleteitemService {
	public deleteitemEvent:EventEmitter<any> = new EventEmitter();

  constructor() { }
}
