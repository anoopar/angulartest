import { Component,OnInit } from '@angular/core';
import { DeleteitemService } from "./deleteitem.service";

@Component({
	selector: '[app-root]',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	public form_elements:any[] = [
			{
				"col_type":"col-2",
				"elements":[
					{
						"type":"text",
						"label":"First Name",
						"required":true,
						"element_id":"100"
					},
					{
						"type":"text",
						"label":"Last Name",
						"required":true,
						"element_id":"110"
					}
				]
			},
			{
				"col_type":"col-1",
				"elements":[
					{
						"type":"email",
						"label":"Email",
						"required":true,
						"element_id":"120"
					}
				]
			},
			{
				"col_type":"col-1",
				"elements":[
					{
						"type":"phone",
						"label":"Mobile",
						"required":false,
						"element_id":"130"
					}
				]
			}
		]
	constructor(
		private dservice:DeleteitemService
	){
	}
	ngOnInit():void{
		this.dservice.deleteitemEvent.subscribe((data)=>{
			this.form_elements = this.form_elements.filter((item) =>{
				item.elements =	item.elements.filter((elem:any)=>{
					if(elem.element_id != data.eid){
						return item
					}

				})
				return item
			})
			console.log(this.form_elements)

		})
	}
	title = 'angulartest';
	getFields(){
		return this.form_elements;
	}
}
